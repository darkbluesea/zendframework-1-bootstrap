<?php

include_once('AbstractMapper.php');

class Application_Model_DBExtras extends Abstract_Mapper
{
	public function __construct()
	{
		$item           = new Application_Model_DbTable_Extras();
		$this->_dbTable = $item;
        $this->Id       = 'idextras';
	}

	public function update_fields($iditem, $params)
	{
		$this->updateInRow($iditem, $this->Id , $params);
	
	}
	
	public function save_item($title, $description)
	{
    try{
        $AuxTools   = new Application_Model_AuxTools();
        $seotitle   = $AuxTools->clear_name($title);

        $params["title"] 		= $title;
        $params["description"]  = $description;
        $params["date_add"]     = new Zend_Db_Expr("NOW()");
        $params["seotitle"]     = $seotitle;
        $this->saveInRow($params);
      return TRUE;

    }catch (Exception $e){
      error_log($e);
      return FALSE;
    }
 	}

 	public function listAll()
 	{

    $db       = $this->getDbTable();
    $select   = $db->select()->order(array($this->Id . ' DESC'));
    $result   = $db->fetchAll($select);

 		return $result;
 	}

 	
 	public function findItem($str)
 	{
 		$str="%".$str."%";
 	
 		$db = $this->getDbTable();
 		$where[] = $db->getAdapter()->quoteInto("title like ?", $str);
 		$result = $db->fetchAll($where);
 		
 		if(is_null($result))
 		{
 			$db = $this->getDbTable();
 			$where[] = $db->getAdapter()->quoteInto("description like ?", $str);
 			$result = $db->fetchAll($where);
  		}
 		return $result;
 	}

  public function getSpecificItemByName($item_name)
  {
    $db = $this->_dbTable;
    $select = $db->select()->where('seotitle = ?', $item_name);
    $result = $db->fetchRow($select);
    return $result;
  }
 	
 	public function getSpecificItem($itemid)
 	{
 		$db = $this->_dbTable;
 		$select = $db->select()->where($this->Id.' = ?', $itemid);
 		$result = $db->fetchRow($select); 	
 		return $result;
 	}

	public function total()
	{
		$db = $this->getDbTable();
		$select = $db->select()->from($db, array('count(*) as amount'));
		$result = $db->fetchRow($select);
		return $result->amount;
	}

 
	public function deleteItem($itemid)
	{
		$db = $this->_dbTable;
		$db->delete(array(
      $this->Id.' = ?' => $itemid
		));
		return !$this->checkItemExistsById($itemid);
	}

	public function checkItemExistsById($itemid)
	{
		$db = $this->_dbTable;
		$select = $db->select()->where($this->Id.' = ?', $itemid);
		$result = $db->fetchRow($select);
		if(is_null($result))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

  /**
   * Old Code for random manipulation
   */
  private function getRandomItemExcluding($itemid)
  {
    $db = $this->getDbTable();
    $query_one 	= "SELECT FLOOR(RAND() * COUNT(*)) AS `offset` FROM `news` where idnews != $itemid";
    $result_one = $db->getDefaultAdapter()->query($query_one)->fetchAll();
    $offset 	= $result_one[0]['offset'];

    $query_two	= "SELECT * FROM `news` LIMIT $offset, 1";
    $result_two = $db->getDefaultAdapter()->query($query_two)->fetchAll();

    //echo "<pre>"; var_dump($result_two[0]); echo "</pre>"; die();
    return $result_two[0];
  }

  private function getRandomItem()
  {
    $db = $this->getDbTable();
    $query_one 	= "SELECT FLOOR(RAND() * COUNT(*)) AS `offset` FROM `news`";
    $result_one = $db->getDefaultAdapter()->query($query_one)->fetchAll();
    $offset 	= $result_one[0]['offset'];

    $query_two	= "SELECT * FROM `news` LIMIT $offset, 1";
    $result_two = $db->getDefaultAdapter()->query($query_two)->fetchAll();

    //echo "<pre>"; var_dump($result_two[0]); echo "</pre>"; die();
    return $result_two[0];
  }
}
<?php

class Abstract_Mapper
{
    
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_User');
        }
        return $this->_dbTable;
    }
 
    public function save($row)
    {    	
        if (null === ($id = $row->get('id'))) {
            unset($row['id']);
            $this->getDbTable()->insert($row);
        } else {
            $this->getDbTable()->update($row, array('id = ?' => $id));
        }
    }

    public function saveInRow($params)
    {
    	$task = $this->_dbTable;
    	$row = $task->createRow($params);
     	return $row->save();
     }
 
    public function find($id)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        
        return $resultSet;
    }
    
    public function now()
    {
    	return date('Y-m-d H:i:s');
    }    
    
    public function updateInRow($id, $idName, $params)
    {
    	$table = $this->_dbTable;
    
    	$where = " $idName = $id ";
    	$row = $table->fetchRow($where);
    	$row->setFromArray($params);
    	$row->save();
    }
}

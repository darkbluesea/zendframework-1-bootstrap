<?php    

class Application_Adapter_BarebonesAuthAdapter implements Zend_Auth_Adapter_Interface
{
	private $_email;
	private $_pass;

	public function init($_email, $_password)
	{
		$this->_email   = $_email;
		$this->_pass    = $_password;
	}

    public function authenticate()
    {
        $AuxTools = new Application_Model_AuxTools();
        $DBUsers  = new Application_Model_DBUsers();
        $result   = false;
        $user     = $DBUsers->getSpecificItemByName($this->_email);

        if(isset($user) && isset($user->password))
        {
            $password   = $AuxTools->encode($this->_pass);
            if (
                $password     == $user->password &&
                $this->_email == $user->email
            ){
                $result = true;
            }

        }

        if( $result	){
            return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $user);
        } else{
            return new Zend_Auth_Result(Zend_Auth_Result::FAILURE, null);
        }
    }
}

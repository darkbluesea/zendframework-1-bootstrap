<?php

class IndexController extends Zend_Controller_Action
{
	protected $_sessionObj;
	
	public function init()
	{
		$this->_sessionObj = new Zend_Session_Namespace('barebones');

        $this->_helper->contextSwitch()
            ->addActionContext('json', array('json'))
            ->setAutoJsonSerialization(true)
            ->initContext();
	}

	public function indexAction()
	{
        $this->view->customKeywords     = "Palavras chave relacionadas como content";
        $this->view->customDescription  = "Descrição relacionada com o content";
	}


    public function jsonAction()
    {
        $example = array(
            "xpto" => "expensive",
            "alfa" => 2,
        );

        $this->view->example = $example;
    }
}

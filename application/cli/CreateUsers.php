<?php

class Application_Cli_CreateUsers
{

    public function __construct($zend = null)
    {
        if (isset($zend)) {
            $this->_zend = $zend->bootstrap();
        }

    }

    public function process($args = null)
    {
        $users = new Application_Model_DBUsers();
        $users->save_item("naia.luis@gmail.com", "testes");
    }
}

// Bootstrap ZendFW1

$cur_dir    = getcwd();
$path_dir   = substr($cur_dir, 0, (strlen($cur_dir) - strlen("/aplication/cli")));
$real_path  = realpath($path_dir . "/application");

// Define path to application directory
defined('APPLICATION_PATH')
|| define('APPLICATION_PATH',
realpath($real_path));

// Define path to basepath directory
defined('BASE_PATH')
|| define('BASE_PATH', realpath($path_dir));

// Define application environment
defined('APPLICATION_ENV')
|| define('APPLICATION_ENV',
(getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV')
    : 'production'));

// Ensure library/ is on include_path
set_include_path(
    implode(
        PATH_SEPARATOR,
        array(
            realpath(APPLICATION_PATH . '/../library'),
            get_include_path(),
        )
    )
);

$config_path = '/configs/application.ini';
if (PHP_OS == 'WINNT' || PHP_OS == 'WIN32') {
    $config_path = str_replace('/', '\\', $config_path);
}

require_once 'Zend/Application.php';
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . $config_path
);

$me = new Application_Cli_CreateUsers($application);
$me->process($argv);


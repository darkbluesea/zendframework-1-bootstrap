<?php

//process stuff here

class Application_Cli_TestDB
{

	public function __construct($zend = null)
	{
		if(isset($zend))
		{
			$this->_zend = $zend->bootstrap();
		}

		$translator					= Zend_Registry::get('Zend_Translate');
		$this->_tranlatorAdapter 	= $translator->getAdapter();
		$this->_MailerGeneralConfig = Zend_Registry::get('config')->mailer;
    $this->pass = 0;
    $this->fail = 0;
    $this->report = array();
    assert_options(ASSERT_WARNING, 0);
	}

  public function __destruct() {
    $total = ($this->pass + $this->fail);
    echo "\n*********************************\n";
    foreach($this->report as $item){
      echo $item ."\n";
    }
    echo "Total tests: \t" . $total . "\n";
    echo "Tests passed: \t" . $this->pass . " (". ($this->pass*100.0/$total) . "%)\n";
    echo "Tests failed: \t" . $this->fail . " (". ($this->fail*100.0/$total) . "%)\n";
  }

	public function process($args = null)
	{
    /*
     * CATEGORIAS
     */
    $cat = new Application_Model_DBCategorias();
    echo "\nCategorias:\n";
    foreach($cat->listAll() as $obj){
      echo $obj->idcategorias . "\t";
      echo $obj->title . "\t";
      echo $obj->description . "\n";
    }
    /*$cat->save_item($this->getrandomstring(8), $this->getrandomstring(20));
   */

    /*
     * VIATURAS
     *  Application_Model_DBViaturas::save_item($title = '', $description = '', $preco = '', $ano = '', $kms = '', $marca = '',
     */
    $via = new Application_Model_DBViaturas();
    $price = rand(1000,20000).".".rand(1,99);
    /* $via->save_item(
      $this->getrandomstring(7),
      $this->getrandomstring(200),
      $price,
      rand(1990,2013),
      rand(100000,500000),
      $this->getrandomstring(14)
    );
    */
    echo "\nViaturas:\n";
    foreach($via->listAll() as $obj){
      echo $obj['viatura']->idviaturas . "\t";
      echo $obj['viatura']->title ."\t";
      echo substr($obj['viatura']->description,0,3) ."\t";
      echo $obj['viatura']->preco ."\t";
      echo $obj['viatura']->ano ."\t";
      echo $obj['viatura']->kms ."\t";
      echo $obj['viatura']->marca ."\t";
      echo $obj['viatura']->hidden ."\t";
      echo "categorias: \n";
      foreach($obj['categorias'] as $item){
        echo " categoria: " . $item->title . "\t\n";
      }
    }

    $viaCat = new Application_Model_DBViaturasCategorias();
    echo "\n Viaturas Categorias:\n";
    foreach($viaCat->listAll() as $obj){
      echo $obj->idcategoria_rel . "\t";
      echo $obj->idviatura_rel . "\n";
    }

    //test FK failure
    $this->checkAssert($viaCat->save_item(13333,23), true, "Testing Foreign key failure");

    //test if it exists, if not creates, on success true
    assert($viaCat->save_item(2,10) === true);

    assert($viaCat->save_item(5,23) === true);
    assert($viaCat->save_item(5,21) === true);
    assert($viaCat->save_item(5,20) === true);

    assert($viaCat->save_item(12,23) === true);
    assert($viaCat->save_item(12,19) === true);

    assert($viaCat->save_item(7,19) === true);
    assert($viaCat->save_item(7,22) === true);
    assert($viaCat->save_item(7,23) === true);
	}

  private function checkAssert($var, $expected, $desc)
  {
    if(assert($var === $expected)){
      $this->pass++;
    }else{
      $this->fail++;

      $bt = debug_backtrace();
      $caller = array_shift($bt);

      $this->report[] = "Failed at line: " . $caller['line'] . " - " . $desc;
    }
  }

  private function getrandomstring($length) {

    global $template;
    settype($template, "string");

    $template = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    /* this line can include numbers or not */

    settype($length, "integer");
    settype($rndstring, "string");
    settype($a, "integer");
    settype($b, "integer");

    for ($a = 0; $a <= $length; $a++) {
      $b = rand(0, strlen($template) - 1);
      $rndstring .= $template[$b];
    }

    return $rndstring;
  }

}

if(isset($argv) && in_array("-linux",$argv))
{
	$cur_dir 	= getcwd();
	$path_dir 	= substr($cur_dir, 0, (strlen($cur_dir) - strlen("/aplication/cli")));
	$real_path 	= realpath($path_dir . "/application");

	// Define path to application directory
	defined('APPLICATION_PATH')
	|| define('APPLICATION_PATH',
	realpath($real_path));

	// Define path to basepath directory
	defined('BASE_PATH')
	|| define('BASE_PATH', realpath($path_dir));

	// Define application environment
	defined('APPLICATION_ENV')
	|| define('APPLICATION_ENV',
	(getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV')
	: 'production'));

	// Ensure library/ is on include_path
	set_include_path(
	implode(
	PATH_SEPARATOR, array(
	realpath(APPLICATION_PATH . '/../library'),
	get_include_path(),
	)
	)
	);

	$config_path = '/configs/application.ini';
	if (PHP_OS == 'WINNT' || PHP_OS == 'WIN32') {
		$config_path = str_replace('/', '\\', $config_path);
	}

	require_once 'Zend/Application.php';
	$application = new Zend_Application(
			APPLICATION_ENV,
			APPLICATION_PATH . $config_path
	);

	$me = new Application_Cli_TestDB($application);
	$me->process($argv);
}

